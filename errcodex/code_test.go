package codex

import (
	"fmt"
	"testing"
)

func TestErrToCode(t *testing.T) {
	Register(map[int64]map[string]string{200: {"zh": "成功", "en": "ok"}})
	lang := Int64(ErrToCode(OK)).Message("en")
	fmt.Println(lang)
}
