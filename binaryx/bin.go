package binaryx

import (
	"fmt"
	"math"
	"strconv"
)

func BinTrans(auto, bigGroup int64) int64 {
	//公式:Bit = 2^((max(auto) - big_group*64) - 1)
	index := (auto - bigGroup*64) - 1
	fmt.Println("index", index)
	pow := math.Pow(2, float64(index))
	fmt.Println(pow)

	s2 := strconv.FormatInt(int64(pow), 2) // 10 to 2
	fmt.Printf("二进制是：%v\n", s2)
	return int64(pow)
}
