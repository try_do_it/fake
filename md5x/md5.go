package md5x

import (
	"crypto/md5"
	"fmt"
	"io/ioutil"
	"os"
)

// md5x str
func Md5String(str string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(str)))
}

func GetMd5Sum(data []byte) string {
	return fmt.Sprintf("%x", md5.Sum(data))
}

func GetFileMd5Sum(path string) (string, error) {
	file, err := os.Open(path)
	if err != nil {
		return "", err
	}

	data, err := ioutil.ReadAll(file)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%x", md5.Sum(data)), nil
}
