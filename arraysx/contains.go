package arraysx

func Contains[T comparable](val T, array []T) int {
	for i, e := range array {
		if e == val {
			return i
		}
	}
	return -1
}
