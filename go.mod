module gitee.com/try_do_it/fake

go 1.18

require (
	github.com/bwmarrin/snowflake v0.3.0
	github.com/golang-jwt/jwt/v4 v4.4.3
	github.com/pkg/errors v0.9.1
	google.golang.org/grpc v1.51.0
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-cmp v0.5.9 // indirect
	golang.org/x/net v0.5.0 // indirect
	google.golang.org/genproto v0.0.0-20230106154932-a12b697841d9 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)
