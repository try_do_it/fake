package paginatorx

const DefaultOrderBy = " create_unix desc"

func Limit(size int64) int64 {
	if size <= 0 {
		return 10
	}
	return size
}

func Offset(num, size int64) int64 {
	offset := (num - 1) * size
	if offset <= 0 {
		return 0
	}
	return offset
}

func Int64ToIter(ids []int64) (iter []interface{}) {
	for _, id := range ids {
		iter = append(iter, id)
	}
	return
}

func OrderBy(orderBy string) string {
	if orderBy == "" {
		return DefaultOrderBy
	}
	return orderBy
}
