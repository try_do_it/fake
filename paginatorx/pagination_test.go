package paginatorx

import (
	"reflect"
	"testing"
)

func TestInt64ToInter(t *testing.T) {
	type args struct {
		ids []int64
	}
	tests := []struct {
		name     string
		args     args
		wantIter []interface{}
	}{
		{name: "", args: args{ids: []int64{1, 2, 3}}, wantIter: []interface{}{1, 2, 3}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotIter := Int64ToIter(tt.args.ids); !reflect.DeepEqual(gotIter, tt.wantIter) {
				t.Errorf("Int64ToInter() = %v, want %v", gotIter, tt.wantIter)
			}
		})
	}
}

func TestLimit(t *testing.T) {
	type args struct {
		size int64
	}
	tests := []struct {
		name string
		args args
		want int64
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Limit(tt.args.size); got != tt.want {
				t.Errorf("Limit() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOffset(t *testing.T) {
	type args struct {
		num  int64
		size int64
	}
	tests := []struct {
		name string
		args args
		want int64
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Offset(tt.args.num, tt.args.size); got != tt.want {
				t.Errorf("Offset() = %v, want %v", got, tt.want)
			}
		})
	}
}
