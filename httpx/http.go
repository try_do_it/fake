package httpx

import (
	"context"
	"fmt"
	"io"
	"net/http"
)

func GetWithContext(ctx context.Context, uri string) ([]byte, error) {
	request, err := http.NewRequestWithContext(ctx, http.MethodGet, uri, nil)
	if err != nil {
		return nil, err
	}
	resp, err := http.DefaultClient.Do(request)
	if err != nil {
		return nil, err
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			return
		}
	}(resp.Body)
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("http get error: uri=%v ,status_code=%v", uri, resp.StatusCode)
	}
	return io.ReadAll(resp.Body)
}
