package jwtx

import (
	"github.com/golang-jwt/jwt/v4"
)

type Claims struct {
	Uid int64 `json:"uid"`
	jwt.MapClaims
}

func ParseMap(token, salt string) (map[string]interface{}, error) {
	var err error
	jwtParser := jwt.NewParser(jwt.WithJSONNumber())

	parse, err := jwtParser.Parse(token, func(token *jwt.Token) (interface{}, error) {
		return []byte(salt), nil
	})
	if err != nil {
		return nil, err
	}
	claims := parse.Claims.(jwt.MapClaims)
	return claims, nil
}

func CreateMap(salt string, payloads map[string]interface{}) (string, error) {
	claims := make(jwt.MapClaims)
	for k, v := range payloads {
		claims[k] = v
	}
	token := jwt.New(jwt.SigningMethodHS256)
	token.Claims = claims
	return token.SignedString([]byte(salt))
}
