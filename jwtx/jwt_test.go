package jwtx

import (
	"fmt"
	"testing"
)

func TestParseMap(t *testing.T) {
	token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOiIxNjgxMzc4OTIwIiwiaWF0IjoiMTY4MTM3NTMyMCIsIm5iZiI6IjE2ODEzNzUzMjAiLCJ1c2VyX2lkIjoiNDIxODgxNTYwMjk2NDg4OTcifQ.0MdFLPeul3bjowo--P_RiiPSdd95ye99VJUEPtkculQ"
	salt := "P8rPGgYA0RsG"
	parseMap, err := ParseMap(token, salt)
	fmt.Println(parseMap, err)
}
