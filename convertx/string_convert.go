package convertx

import (
	"reflect"
	"strconv"
	"unsafe"
)

func Float64ToString(v float64) string {
	return strconv.FormatFloat(v, 'f', -1, 64)
}

// StringToInt @Converter implicit
func StringToInt(str string) int {
	result, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		return 0
	}
	return int(result)
}

// StringToInt32 @Converter implicit
func StringToInt32(str string) int32 {
	result, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		return 0
	}
	return int32(result)
}

func StringToFloat64(str string) float64 {
	float, err := strconv.ParseFloat(str, 64)
	if err != nil {
		return 0
	}
	return float
}

// @Converter implicit
func StringToInt64(str string) int64 {
	result, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		return 0
	}
	return result
}

// @Converter implicit
func IntToString(val int) string {
	return strconv.FormatInt(int64(val), 10)
}

// @Converter implicit
func Int32ToString(val int32) string {
	return strconv.FormatInt(int64(val), 10)
}

// @Converter implicit
func Int64ToString(val int64) string {
	return strconv.FormatInt(int64(val), 10)
}

func UnsafeStringToByte(s string) []byte {
	return (*[0x7fff0000]byte)(unsafe.Pointer(
		(*reflect.StringHeader)(unsafe.Pointer(&s)).Data),
	)[:len(s):len(s)]
}
