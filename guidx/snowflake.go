package guidx

import (
	"github.com/bwmarrin/snowflake"
)

type IdGenerator struct {
	node *snowflake.Node
}

func NewIdGenerator(nodeId int64) *IdGenerator {
	instance := &IdGenerator{}
	snowflake.Epoch = 1641135917000 //ms 2022-01-02 23:05:17
	var err error

	snowflake.NodeBits = 6
	snowflake.StepBits = 14
	node, err := snowflake.NewNode(nodeId)
	if err != nil {
		panic(err)
	}

	instance.node = node
	return instance
}

func (s *IdGenerator) GetId() int64 {
	return s.node.Generate().Int64()
}
func (s *IdGenerator) GetIdList(len int) []int64 {

	res := make([]int64, 0)
	for i := 0; i < len; i++ {
		res = append(res, s.node.Generate().Int64())
	}

	return res
}
